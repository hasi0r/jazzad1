package servlets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64.InputStream;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;
import com.itextpdf.tool.xml.XMLWorkerHelper;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

	/**
	 * e
	 * 
	 */
	private static final long serialVersionUID = 1L;

	double dKwota;
	double dOprocentowanie;
	double dstala;
	int iIleRat;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String pdf ="";
		

			String kwota = request.getParameter("kwota");
			String ileRat = request.getParameter("ileRat");
			String oprocentowanie = request.getParameter("oprocentowanie");
			String stala = request.getParameter("stala");
			String rodzaj = request.getParameter("rodzaj");
			 Document document = new Document();

			
			
			
			
			if (kwota.equals(null) || kwota.equals("") || ileRat.equals(null) || ileRat.equals("")
					|| oprocentowanie.equals(null) || oprocentowanie.equals("") || stala.equals(null)
					|| stala.equals("") || rodzaj.equals(null) || rodzaj.equals("")) {
				response.setContentType("text/html");

				response.getWriter().println("<h1>Zle dane!</h1>");
			}

			try {
				dKwota = Double.valueOf(kwota.trim()).doubleValue();
				dOprocentowanie = Double.valueOf(oprocentowanie.trim()).doubleValue();
				dstala = Double.valueOf(stala.trim()).doubleValue();
				iIleRat = Integer.valueOf(ileRat.trim()).intValue();

			} catch (NumberFormatException nfe) {
				response.setContentType("text/html");
				response.getWriter()
						.println("<h1>Kwota, ilosc rat, oprocentowanie, oplata stala musza byc liczbami!</h1>");
			}

			// tabela
			response.setContentType("text/html");
			String tempStr="<h1>Wyliczenie rat kredytu: </h1><b>Wartosc kredytu: </b><i>" + dKwota
							+ "</i><b>, oprocentowanie: " + dOprocentowanie + ",<br /> ilosc rat:"
							+ iIleRat + ",<br /> raty:</b>";
			System.out.println(tempStr);
			response.getWriter()
					.print(tempStr);
			
			pdf = pdf+tempStr; // dodanie ciagu pdf
			FileOutputStream file = new FileOutputStream(new File("E:\\Test.pdf"));
			try {
			    String k = pdf;
			    
			    ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
			    PdfWriter writer = PdfWriter.getInstance(document, file);
			    
			    document.open();
			    
			    XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
			    
			   
			} catch (Exception e) {
			    e.printStackTrace();
			}
			
			//PdfPTable table = new PdfPTable(5);
			//table.setHeaderRows();
	       // table.setSplitRows(false);
	       // table.setComplete(false);
			 
		 
	        


			tempStr="<table border=1><tr><td>Numer raty</td><td>Kwota kapitalu</td><td>Kwota odesetek</td><td>Oplaty stale</td>"
					+ "<td>Calkowita kwota raty</td></tr>";
			response.getWriter().print(tempStr);
			pdf = pdf+tempStr; // dodanie ciagu pdf
		
			if (rodzaj.equals("stala")) {
				response.getWriter().print(" rowne.</p>");
				PdfPTable table = new PdfPTable(5);
		        table.setHeaderRows(1);
		        table.setSplitRows(false);
		        table.setComplete(false);
		 

		        table.addCell("Numer raty");
				table.addCell("Kwota kapitalu");
				table.addCell("Kwota odesetek");
				table.addCell("Oplaty stale");
				table.addCell("Calkowita kwota raty");
		 
		        for (int i = 0; i < iIleRat; i++) {
		        	table.addCell(String.valueOf(i));
					table.addCell("obliczona kwota kapitalu");
					table.addCell("Kwota odesetek");
					table.addCell("Oplaty stale");
					table.addCell("Calkowita kwota raty");
				
					tempStr ="<tr><td>" + i + "</td><td>" + "obliczona kwota kapitalu" + "</td><td>"
							+ "Kwota odesetek" + "</td><td>" + "Oplaty stale" + "</td><td>"
							+ "Calkowita kwota raty" + "</td>";
					response.getWriter()
							.print(tempStr);
		        	
		            try {
						document.add(table);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }

		        table.setComplete(true);
		        try {
					document.add(table);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        document.close();
				
			} else { // TODO obliczenia rat
				response.getWriter().print(" malejace.");
				for (int i = 1; i <= iIleRat; i++) {
				tempStr="<tr><td>" + i + "</td><td>" + "obliczona kwota kapitalu" + "</td><td>"
						+ "Kwota odesetek" + "</td><td>" + "Oplaty stale" + "</td><td>"
						+ "Calkowita kwota raty" + "</td>";
					response.getWriter()
							.print(tempStr);
					pdf = pdf+tempStr; // dodanie ciagu pdf
				}
			}
			response.getWriter().println("</tr></table>");

		
			
			 document.close();
			    file.close();
		
		
	}

}
